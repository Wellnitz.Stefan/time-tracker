# time-tracker
track times you work on projects via simple cli-tool
 - controlled by configuration file
 - stores data in sqlite database (TBD)

examplesto get an idea... (TBD!):
```bash
time-tracker --project "My project"
 project "My project" set as active project

time-tracker --start
 started tracking time for project "My project"

time-tracker --start
 tracking time for project "My project" already started

time-tracker --stop
 stopped tracking time for project "My project", you worked 1h 30m

time-tracker --stop
 tracking time for project "My project" not started

time-tracker --project "My other project"
 project "My other project" set as active project

time-tracker --list-projects
 available projects:
 - My project
 - My other project

time-tracker --stats this_week
 active project: My other project
 2024-04-10, wednesday: 1h 30m
 2024-04-09, tuesday:   2h 15m
 2024-04-08, monday:    0h 45m

time-tracker --stats this_month
 active project: My other project
 CW 14: 40h 30m

time-tracker --stats this_year
 active project: My other project
 April 14: 20d 4h 30m
 March 14: 20d 4h 30m
 February 14: 20d 4h 30m
 January 14: 20d 4h 30m

time-tracker --stats contributionsgraph
 active project: My other project
 Jan  Feb  Mar  Apr  May  Jun  Jul  Aug  Sep  Oct  Nov  Dec
 .oOo
 o.
 X
 o
 .



time-tracker --contributionsgraph --week
 active project: My other project
 ...
```

## Getting started
In this section, we will guide you through the process of setting up the project on your local machine.
We recommend using Docker to build and run the project.
### Install Docker
#### Setup Dockers apt-repository.
```bash
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  bookworm stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

#### Install Docker packages
```bash
sudo apt-get install docker-ce \
                     docker-ce-cli \
                     containerd.io \
                     docker-buildx-plugin \
                     docker-compose-plugin
```

#### Add user to docker group
```bash
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
```

#### Enable Docker service
```bash
sudo systemctl enable docker.service
sudo systemctl start docker.service
```

#### (optional) Testing the Docker installation
Obligatory hello-world test
```bash
docker run hello-world
```

### Building the source code
Building the image, this may take some time. Don't miss the dot at the end.
```bash
docker build -t my-time-tracker-image .
```

Start the image as container and login
```bash
docker run -it my-time-tracker-image
```

## ArgumentParser

### Class diagramm
```plantuml
class ArgumentParser {
    - parsedArguments_: std::unordered_map<std::string, std::string>
    - requiredKeys_: std::vector<std::string>
    --------------------
    + ArgumentParser(argc: int, argv: char*[])
    + hasKey(key: const std::string&): bool
    + getValue(key: const std::string&): std::optional<std::string>
    + printArguments(): void
    + registerRequiredKeys(keys: const std::vector<std::string>&): bool
    - parseArguments(argc: int, argv: char*[]): void
}
```
## ConfigFileReader
```
[SETTING]
dbfile=time-tracker.db
logfile=time-tracker.log
loglevel=INFO
lang=de_DE
```

## Logger
The `Logger` class is responsible for logging messages in a software application. It provides various logging levels, such as `TRACE`, `DEBUG`, `INFO`, `WARN`, `ERROR`, and `FATAL`, to categorize the severity of the logged messages.

The class has a singleton design pattern, meaning that there can only be one instance of the `Logger` class throughout the application. You can access this instance using the `getInstance()` method.

The `Logger` class has methods to set the logging level and the file name for the log file. It also provides methods to log messages at different levels, such as `trace()`, `debug()`, `info()`, `warn()`, `error()`, and `fatal()`. Each of these methods takes a message as input and logs it with the corresponding logging level.

Internally, the `Logger` class maintains a logging level, a file name, and a log file object. It also uses a mutex to ensure thread safety when multiple threads are logging messages simultaneously.

Additionally, the `Logger` class provides utility methods like `getTime()` to get the current timestamp and `logLevelToString()` to convert a logging level enum value to a string representation.

Overall, the `Logger` class is a crucial component for monitoring and debugging a software application, allowing developers to track the flow of execution and identify potential issues.

### Class diagramm

```plantuml
class Logger {
    - m_loggingLevel: LogLevel
    - m_fileName: std::string
    - m_logFile: std::ofstream
    - m_mutex: std::mutex
    --------------------
    + getInstance(): Logger&
    + getTime(): std::string
    + setLoggingLevel(loggingLevel: LogLevel): void
    + setFileName(fileName: std::string): void
    + log(logLevel: LogLevel, message: const std::string&): void
    + trace(message: std::string): void
    + debug(message: std::string): void
    + info(message: std::string): void
    + warn(message: std::string): void
    + error(message: std::string): void
    + fatal(message: std::string): void
    + logLevelToString(logLevel: LogLevel): std::string
    --------------------
    - Logger()
    ~ Logger()
}

enum LogLevel {
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR,
    FATAL
}
```

# database
Since we want to store the data in a database, we need to create a database schema. We will use SQLite as the database engine, as it is lightweight and easy to use.

Now we will define the database schema for our time tracker application. We will create two tables: `projects` and `times`.

This is the first idea of the database schema. We will refine it as we go along.

```plantuml
@startwbs
* tables
** projects
*** id: int
*** name: string
** times
*** id: int
*** project_id: int
*** start_time: timestamp
*** end_time: timestamp
@endwbs
```
## Questions
 - storing linux timestamps in sqlite?
 - using sqlite-timestamp
 