#include <string>
#include <map>
#include <cstdlib>
#include <utility> 

enum class supported_languages {
    en,
    de,
    fr,
    es,
    it
};

enum text_elements {
    PROGRAMM_GREETING,
    PROGRAMM_EXIT,
    CANT_OPEN_FILE,
    CONFIG_HAS_ERRORS,
    KEY_HAS_NO_VALUE,
    KEY_NOT_FOUND,
    SECTION_NOT_FOUND,
    FILE_NOT_FOUND,
    FILE_NOT_READABLE,
    FILE_OPENED_BY_OTHER_APP,
    FILE_EMPTY
};

class LanguageManager {

private:
    static std::map<text_elements, std::string> current_language;

public:
    LanguageManager() {
        current_language = {};
    }
    supported_languages stringToSupportedLanguage(const std::string& langStr);
    bool setLanguage(supported_languages lang);
    std::string getText(const text_elements key);
};