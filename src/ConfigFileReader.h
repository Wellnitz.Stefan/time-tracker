#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>
#include <algorithm>

class ConfigFileReader {
public:

    /**
     * @brief Constructs a ConfigFileReader object with the specified filename.
     * 
     * @param filename The name of the configuration file to read.
     */
    ConfigFileReader(const std::string& filename);

    /**
     * @brief Reads the configuration file and populates the configMap with key-value pairs.
     * 
     * @return true if the file is successfully read and parsed, otherwise false.
     */
    bool readConfigFile();

    /**
     * @brief Retrieves the value associated with the specified key in the given section.
     * 
     * @param section The name of the section where the key is located.
     * @param key The key for which to retrieve the value.
     * @return The value associated with the key if found, otherwise an empty string.
     */
    std::string getValue(const std::string& section, const std::string& key) const;

    /**
     * @brief Checks if all required keys are present and have values in their respective sections.
     * 
     * @return true if all required keys are present with values in their respective sections, otherwise false.
     */
    bool registerRequiredKeys();


private:

    /**
     * @brief A vector of pairs representing the keys and their corresponding sections to be checked.
     * 
     * Each pair consists of the section name as the first element and the key name as the second element.
     */
    std::vector<std::pair<std::string, std::string>> keysInSection = {
        {"SETTINGS", "dbfile"},
        {"SETTINGS", "logfile"},
        {"SETTINGS", "loglevel"},
        {"SETTINGS", "lang"},
    };

    std::string filename;

    /**
     * @brief Checks if a key is present in a given section of the configuration.
     * 
     * @param section The name of the section to search for the key.
     * @param key The key to search for.
     * @return true if the key is found in the specified section, otherwise false.
     */
    bool isKeyPresentInSection(const std::string& section, const std::string& key) const;

    /**
     * @brief A nested unordered map representing the configuration data.
     * 
     * The outer map stores sections where each section is associated with an inner map.
     * The inner map stores key-value pairs where each key-value pair represents a configuration parameter.
     */
    std::unordered_map<std::string, std::unordered_map<std::string, std::string>> configMap;

    /**
     * @brief Parses a line from the configuration file and updates the current section and key-value pairs accordingly.
     * 
     * @param line The line to parse.
     * @param currentSection Reference to the current section name.
     */
    void parseLine(const std::string& line, std::string& currentSection);

    /**
     * @brief Parses a line to extract the section name if found.
     * 
     * @param line The line to parse.
     * @return The section name if a new section is found, otherwise an empty string.
     */
    std::string parseSection(const std::string& line) const;

    /**
     * @brief Parses a line to extract the key-value pair if found.
     * 
     * @param line The line to parse.
     * @return A pair containing the key and value if found, otherwise an empty pair.
     */
    std::pair<std::string, std::string> parseKeyValue(const std::string& line) const;
    
    /**
     * @brief Removes leading and trailing whitespaces from a string.
     * 
     * @param str The string to trim.
     */
    void trim(std::string& str) const;
};
