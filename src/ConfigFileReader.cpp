
#include "ConfigFileReader.h"

ConfigFileReader::ConfigFileReader(const std::string& filename) : filename(filename) {}

bool ConfigFileReader::readConfigFile() {
    // Check if the file exists and is readable
    std::ifstream file(filename);
    if (!file.good()) {
        std::cerr << "Error: Unable to open file." << filename << std::endl;
        return false;
    }

    // Check if the file is opened by another application
    if (file.peek() == std::ifstream::traits_type::eof()) {
        std::cerr << "Error: Unable to open file." << std::endl;
        return false;
    }

    // Reset the file pointer to the beginning
    file.clear();
    file.seekg(0);

    std::string line;
    std::string currentSection;

    // Read each line of the file and parse it
    while (std::getline(file, line)) {
        parseLine(line, currentSection);
    }

    // Close the file
    file.close();
    return true;
}

std::string ConfigFileReader::getValue(const std::string& section, const std::string& key) const {
    // Suchen des angegebenen Abschnitts in der Konfigurationsmappe
    if (auto sectionIter = configMap.find(section); sectionIter != configMap.end()) {
        // Suchen des angegebenen Schlüssels im Abschnitt
        if (auto keyIter = sectionIter->second.find(key); keyIter != sectionIter->second.end()) {
            // Rückgabe des Werts, wenn der Schlüssel gefunden wird
            return keyIter->second;
        }
    }
    // Rückgabe eines leeren Strings, wenn der Abschnitt oder der Schlüssel nicht gefunden wird
    return {};
}


std::string ConfigFileReader::parseSection(const std::string& line) const {
    // Prüfen, ob die Zeile eine Sektion darstellt
    if (line.size() >= 2 && line.front() == '[' && line.back() == ']') {
        // Extrahieren des Sektionsnamens und Rückgabe
        return line.substr(1, line.size() - 2);
    }
    // Rückgabe eines leeren Strings, wenn keine Sektion gefunden wurde
    return {};
}



std::pair<std::string, std::string> ConfigFileReader::parseKeyValue(const std::string& line) const {
    // Finden der Position des ersten '=' Zeichens
    size_t pos = line.find('=');
    // Wenn '=' Zeichen gefunden wird
    if (pos != std::string::npos) {
        // Extrahieren von Schlüssel und Wert aus der Zeile
        std::string key = line.substr(0, pos);
        std::string value = line.substr(pos + 1);
        // Trimmen von führenden und nachfolgenden Leerzeichen von Schlüssel und Wert
        trim(key);
        trim(value);
        // Rückgabe des Schlüssel-Wert-Paares
        return {std::move(key), std::move(value)};
    }
    return {};
}

void ConfigFileReader::parseLine(const std::string& line, std::string& currentSection) {
    // Ignorieren von Zeilen, die mit "#" oder ";" beginnen oder leer sind
    if (line.empty() || line[0] == '#' || line[0] == ';') {
        return;
    }

    // Parsen der Sektion aus der Zeile
    if (auto section = parseSection(line); !section.empty()) {
        currentSection = std::move(section);
        return;
    }

    // Parsen des Schlüssel-Wert-Paares aus der Zeile
    if (!currentSection.empty()) {
        if (auto [key, value] = parseKeyValue(line); !key.empty()) {
            configMap[currentSection][std::move(key)] = std::move(value);
        }
    }
}

bool ConfigFileReader::isKeyPresentInSection(const std::string& section, const std::string& key) const {
    // Verwenden von C++17-Strukturierten Bindungen, um das Ergebnis von find zu extrahieren
    if (auto sectionIter = configMap.find(section); sectionIter != configMap.end()) {
        // Verwenden von C++17-Strukturierten Bindungen, um das Ergebnis von find zu extrahieren
        if (auto keyIter = sectionIter->second.find(key); keyIter != sectionIter->second.end()) {
            return true;
        }
    }
    return false;
}

bool ConfigFileReader::registerRequiredKeys() {
    bool hasAllKeys = true;
    // Verwenden einer Bereichsbasierten for-Schleife, um durch die Schlüssel in den Abschnitten zu iterieren
    for (const auto& [section, key] : keysInSection) {
        // Überprüfen, ob der Schlüssel im angegebenen Abschnitt vorhanden ist
        if (!isKeyPresentInSection(section, key)) {
            hasAllKeys = false;
            std::cerr << "Error: Required key '" << key << "' not found in section '" << section << "'." << std::endl;
        } else {
            // Überprüfen, ob der Schlüssel einen Wert hat
            const auto& value = configMap[section][key];
            if (value.empty()) {
                hasAllKeys = false;
                std::cerr << "Error: Required key '" << key << "' in section '" << section << "' has no value." << std::endl;
            }
        }
    }
    return hasAllKeys;
}

void ConfigFileReader::trim(std::string& str) const {
    // Lambda-Funktion zum Testen, ob ein Zeichen ein Whitespace-Zeichen ist
    auto isWhitespace = [](char c) { return std::isspace(static_cast<unsigned char>(c)); };

    // Verwenden von std::find_if, um das erste nicht-Whitespace-Zeichen zu finden
    auto start = std::find_if(str.begin(), str.end(), isWhitespace);

    // Verwenden von std::find_if_not, um das letzte nicht-Whitespace-Zeichen zu finden
    auto end = std::find_if_not(str.rbegin(), str.rend(), isWhitespace).base();

    // Verwenden von std::string::erase, um die Leerzeichen am Anfang zu entfernen
    str.erase(str.begin(), start);

    // Verwenden von std::string::erase, um die Leerzeichen am Ende zu entfernen
    str.erase(end, str.end());
}



