#include "LanguageManager.h"

std::map<text_elements, std::string> LanguageManager::current_language;

std::map<text_elements, std::string> texts_de = {
    {PROGRAMM_GREETING, "Hallo! Willkommen im Programm."},
    {PROGRAMM_EXIT, "Programm wird beendet."},
    {CANT_OPEN_FILE, "Datei konnte nicht geöffnet werden."},
    {CONFIG_HAS_ERRORS, "Configuration file has errors."},
    {KEY_HAS_NO_VALUE, "Schlüssel hat keinen Wert."},
    {KEY_NOT_FOUND, "Schlüssel nicht gefunden."},
    {SECTION_NOT_FOUND, "Sektion nicht gefunden."},
    {FILE_NOT_FOUND, "Datei nicht gefunden."},
    {FILE_NOT_READABLE, "Datei nicht lesbar."},
    {FILE_OPENED_BY_OTHER_APP, "Datei von einer anderen Anwendung geöffnet."},
    {FILE_EMPTY, "Datei ist leer."}
};

std::map<text_elements, std::string> texts_en = {
    {PROGRAMM_GREETING, "Hello! Welcome to the program."},
    {PROGRAMM_EXIT, "Exiting program."},
    {CANT_OPEN_FILE, "Unable to open file."},
    {CONFIG_HAS_ERRORS, "Configuration file has errors."},
    {KEY_HAS_NO_VALUE, "Key has no value."},
    {KEY_NOT_FOUND, "Key not found."},
    {SECTION_NOT_FOUND, "Section not found."},
    {FILE_NOT_FOUND, "File not found."},
    {FILE_NOT_READABLE, "File not readable."},
    {FILE_OPENED_BY_OTHER_APP, "File opened by another application."},
    {FILE_EMPTY, "File is empty."}
};

std::map<text_elements, std::string> texts_es = {
    {PROGRAMM_GREETING, "¡Hola! Bienvenido al programa."},
    {PROGRAMM_EXIT, "Saliendo del programa."},
    {CANT_OPEN_FILE, "No se puede abrir el archivo."},
    {CONFIG_HAS_ERRORS, "MISSING"},
    {KEY_HAS_NO_VALUE, "La clave no tiene valor."},
    {KEY_NOT_FOUND, "Clave no encontrada."},
    {SECTION_NOT_FOUND, "Sección no encontrada."},
    {FILE_NOT_FOUND, "Archivo no encontrado."},
    {FILE_NOT_READABLE, "Archivo no legible."},
    {FILE_OPENED_BY_OTHER_APP, "Archivo abierto por otra aplicación."},
    {FILE_EMPTY, "El archivo está vacío."}
};

std::map<text_elements, std::string> texts_fr = {
    {PROGRAMM_GREETING, "Bonjour! Bienvenue dans le programme."},
    {PROGRAMM_EXIT, "Fermeture du programme."},
    {CANT_OPEN_FILE, "Impossible d'ouvrir le fichier."},
    {CONFIG_HAS_ERRORS, "MISSING"},
    {KEY_HAS_NO_VALUE, "La clé n'a pas de valeur."},
    {KEY_NOT_FOUND, "Clé non trouvée."},
    {SECTION_NOT_FOUND, "Section non trouvée."},
    {FILE_NOT_FOUND, "Fichier non trouvé."},
    {FILE_NOT_READABLE, "Fichier non lisible."},
    {FILE_OPENED_BY_OTHER_APP, "Fichier ouvert par une autre application."},
    {FILE_EMPTY, "Le fichier est vide."}
};

std::map<text_elements, std::string> texts_it = {
    {PROGRAMM_GREETING, "Ciao! Benvenuto nel programma."},
    {PROGRAMM_EXIT, "Uscita dal programma."},
    {CANT_OPEN_FILE, "Impossibile aprire il file."},
    {CONFIG_HAS_ERRORS, "MISSING"},
    {KEY_HAS_NO_VALUE, "La chiave non ha valore."},
    {KEY_NOT_FOUND, "Chiave non trovata."},
    {SECTION_NOT_FOUND, "Sezione non trovata."},
    {FILE_NOT_FOUND, "File non trovato."},
    {FILE_NOT_READABLE, "File non leggibile."},
    {FILE_OPENED_BY_OTHER_APP, "File aperto da un'altra applicazione."},
    {FILE_EMPTY, "Il file è vuoto."}
};


bool LanguageManager::setLanguage(supported_languages lang) {
    if       (lang == supported_languages::en) { current_language = std::move(texts_en);
    } else if(lang == supported_languages::de) { current_language = std::move(texts_de);
    } else if(lang == supported_languages::fr) { current_language = std::move(texts_fr);
    } else if(lang == supported_languages::es) { current_language = std::move(texts_es);
    } else if(lang == supported_languages::it) { current_language = std::move(texts_it);
    } else {
        return false;
    }
    return true;
}

std::string LanguageManager::getText(const text_elements key) {
    auto it = current_language.find(key);
    if (it != current_language.end()) {
        return it->second;
    } else {
        return "TRANSLATION_MISSING!";
    }
}

supported_languages LanguageManager::stringToSupportedLanguage(const std::string& langStr) {
    if (langStr == "en") {
        return supported_languages::en;
    } else if (langStr == "de") {
        return supported_languages::de;
    } else if (langStr == "fr") {
        return supported_languages::fr;
    } else if (langStr == "es") {
        return supported_languages::es;
    } else if (langStr == "it") {
        return supported_languages::it;
    } else {
        return supported_languages::en; 
    }
}