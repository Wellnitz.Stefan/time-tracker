#ifndef COMMANDLINEPARSER_H
#define COMMANDLINEPARSER_H

#include <string>
#include <iostream>
#include <optional>
#include <vector>
#include <unordered_map>

/**
 * @class ArgumentParser
 * @brief A class for parsing command line arguments.
 *
 * This class allows for parsing command line arguments and managing
 * the discovered key and their values.
 */
class ArgumentParser {
public:
    /**
     * @brief Constructor for the ArgumentParser class.
     * @param argc The number of command line arguments.
     * @param argv An array of command line arguments.
     *
     * This constructor takes the number of command line arguments and
     * the array of arguments and parses them to extract key and their
     * values, storing them in an internal unordered_map.
     */
    ArgumentParser(int argc, char* argv[]);

    /**
     * @brief Checks if a specific key exists in the arguments.
     * @param key The key to check.
     * @return true if the key is found, false otherwise.
     *
     * This function checks if the specified key exists in the parsed
     * command line arguments.
     */
    bool hasKey(const std::string& key) const;

    /**
     * @brief Retrieves the value of a specific key.
     * @param key The key for which to retrieve the value from map.
     * @return The value of the key as a string (false if the key is not found).
     *
     * This function returns the value of the specified key if it is present
     * in the parsed command line arguments.
     */
    std::optional<std::string> getValue(const std::string& key) const;

    /**
     * @brief Prints the usage information for the program.
     *
     * This function prints the usage information for the program, including
     * the available options and their descriptions.
     */
    void printUsage() const;

    /**
     * @brief Prints the contents of the parsed command line arguments.
     *
     * This function prints the contents of the internal unordered_map
     * containing the parsed command line arguments, including keys and their values.
     */
    void printArguments() const;

    bool registerRequiredKeys(const std::vector<std::string>& keys);

private:
    std::unordered_map<std::string, std::string> parsedArguments_; /**< A HashMap for storing the parsed command line arguments. */

    /**
     * @brief Parses the command line arguments and stores key and values in the HashMap.
     * @param argc The number of command line arguments.
     * @param argv An array of command line arguments.
     *
     * This function parses the command line arguments and extracts keys and their
     * values, which are then stored in the internal HashMap.
     */
    void parseArguments(int argc, char* argv[]);

    std::vector<std::string> requiredKeys_;
};

#endif // COMMANDLINEPARSER_H
