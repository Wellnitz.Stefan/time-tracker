#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <fstream>
#include <mutex>

enum class LogLevel {
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR,
    FATAL
};

class Logger {
public:
    static Logger& getInstance();
    static std::string logLevelToString(LogLevel logLevel);
    void setLoggingLevel(LogLevel loggingLevel);
    void setFileName(const std::string& fileName);
    LogLevel stringToLogLevel(const std::string& levelString);

    [[maybe_unused]] void trace(const std::string& message);
    [[maybe_unused]] void debug(const std::string& message);
    [[maybe_unused]] void info(const std::string& message);
    [[maybe_unused]] void warn(const std::string& message);
    [[maybe_unused]] void error(const std::string& message);
    [[maybe_unused]] void fatal(const std::string& message);
    
private:
    Logger();
    ~Logger();

    Logger(const Logger&) = delete;
    Logger& operator=(const Logger&) = delete;
    void log(LogLevel level, const std::string& message);
    std::string getTime();

    std::string m_fileName;
    std::ofstream m_logFile;
    std::mutex m_mutex;
    LogLevel m_loggingLevel = LogLevel::TRACE;
};

#endif // LOGGER_H
