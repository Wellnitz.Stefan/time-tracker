#include <iostream>
#include "Logger.h"
#include "ArgumentParser.h"
#include "ConfigFileReader.h"
#include "LanguageManager.h"

#define DEFAULT_CONFIGFILE_PATH "time-tracker.conf"
#define DEFAULT_LANG supported_languages::en
#define PROGRAM_VERSION "0.0.1"

int main(int argc, char** argv) {

    // set language to default language
    LanguageManager lang;
    lang.setLanguage(DEFAULT_LANG);

    // load configuration file from default path
    ConfigFileReader reader(DEFAULT_CONFIGFILE_PATH);
    if (!reader.readConfigFile()) {
        std::cerr << lang.getText(PROGRAMM_EXIT) << std::endl;
        return 1;
    }
    
    // Überprüfe, ob alle erforderlichen Schlüssel in den entsprechenden Sektionen vorhanden sind und Werte haben
    if (!reader.registerRequiredKeys()) {
        std::cerr << lang.getText(CONFIG_HAS_ERRORS) << std::endl;
        return 1;
    }
    
    Logger& logger = Logger::getInstance();

    std::string logfile_name = reader.getValue("SETTINGS", "logfile");
    std::string loglevel_name = reader.getValue("SETTINGS", "loglevel");
    std::string lang_name = reader.getValue("SETTINGS", "lang");
    LogLevel converted_loglevel = logger.stringToLogLevel(loglevel_name);
    supported_languages converted_lang = lang.stringToSupportedLanguage(lang_name);

    logger.setFileName(logfile_name);
    logger.setLoggingLevel(converted_loglevel);
    lang.setLanguage(converted_lang);
       
    logger.info(lang.getText(PROGRAMM_GREETING));

    ArgumentParser parser(argc, argv);
    parser.printArguments();

    /* configure argument parser
    if(!parser.registerRequiredKeys({"--logfile"})) {
        return 1;
    }*/

    logger.info(lang.getText(PROGRAMM_EXIT));
    return 0;
}
