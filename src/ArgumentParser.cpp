#include "ArgumentParser.h"

ArgumentParser::ArgumentParser(int argc, char* argv[]) {
    parseArguments(argc, argv);
}

bool ArgumentParser::hasKey(const std::string& key) const {
    return parsedArguments_.count(key) > 0;
}

std::optional<std::string> ArgumentParser::getValue(const std::string& key) const {
    if(hasKey(key)) {
        return parsedArguments_.at(key);
    }
    return {};
}

void ArgumentParser::printArguments() const {
    for(const auto& pair : parsedArguments_) {
        std::cout << "Key: " << pair.first << ", Value: " << pair.second << std::endl;
    }
}

bool ArgumentParser::registerRequiredKeys(const std::vector<std::string>& keys) {
    bool hasAllArgs = true;
    for(const std::string& key : keys) {
        if(!hasKey(key)) {
            hasAllArgs = false;
            std::cerr << "Error: Required key '" << key << "' not found in command line arguments." << std::endl;
        }
        if(!getValue(key)) {
            hasAllArgs = false;
            std::cerr << "Error: Required key '" << key << "' has no value." << std::endl;
        }
    }
    return hasAllArgs;
}

void ArgumentParser::printUsage() const {
    std::cout << "Usage: program_name [options]" << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "--help\t\t\tDisplay this help message" << std::endl;
    std::cout << "-option1 value1\t\tDescription of option1" << std::endl;
    std::cout << "-option2 value2\t\tDescription of option2" << std::endl;
    std::cout << "-option3 value3\t\tDescription of option3" << std::endl;
    std::cout << "-option4 value4\t\tDescription of option4" << std::endl;
    std::cout << "-option5 value5\t\tDescription of option5" << std::endl;
    std::cout << "-option6 value6\t\tDescription of option6" << std::endl;
    std::cout << "-option7 value7\t\tDescription of option7" << std::endl;
    std::cout << "-option8 value8\t\tDescription of option8" << std::endl;
    std::cout << "-option9 value9\t\tDescription of option9" << std::endl;
    std::cout << "-option10 value10\tDescription of option10" << std::endl;
}

void ArgumentParser::parseArguments(int argc, char* argv[]) {
    for(int i = 1; i < argc; ++i) {
        const std::string arg(argv[i]);
        if(arg == "--help" || arg == "") {
            printUsage(); // Ausgabe der Hilfeinformation
        }
        if(arg.size() > 1 && arg[0] == '-') {
            // key gefunden
            if(i + 1 < argc && argv[i + 1][0] != '-') {
                // value gefunden
                parsedArguments_[arg] = argv[i + 1];
                ++i;
            } else {
                // Kein Wert für den key gefunden
                parsedArguments_[arg] = "";
            }
        }
    }
}