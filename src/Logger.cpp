#include <chrono>
#include <iomanip>
#include <iostream>
#include <sstream>
#include "Logger.h"

Logger& Logger::getInstance() {
    static Logger instance;
    return instance;
}

Logger::Logger() {
}

std::string Logger::getTime() {
        auto now = std::chrono::system_clock::now();
        auto now_time = std::chrono::system_clock::to_time_t(now);
        std::ostringstream oss;
        oss << std::put_time(std::localtime(&now_time), "%Y-%m-%d %H:%M:%S");
        std::string timestamp = oss.str();
        return timestamp;
}

void Logger::setLoggingLevel(LogLevel loggingLevel) {
    m_loggingLevel = loggingLevel;
}

void Logger::setFileName(const std::string& fileName) {
    m_fileName = fileName;
}

void Logger::log(LogLevel logLevel, const std::string& message) {

    if ("" == m_fileName) {
        std::cout << "Logger-Error: file-name not set!" << std::endl;
        return;
    }

    if (logLevel < m_loggingLevel) {
        return;

    } else {

        std::lock_guard<std::mutex> lock(m_mutex);
        
        try {
            m_logFile.open(m_fileName, std::ios::app);
            m_logFile << "[" << getTime() << "] "
                      << "[" << logLevelToString(logLevel) << "] "
                      << message << std::endl;
            m_logFile.close();
        }

        catch(const std::exception& e) {
            std::cout << "Logger-Error: " << e.what() << std::endl;
        }
    }
}

void Logger::trace(const std::string& message) {
    log(LogLevel::TRACE, message);
}

void Logger::debug(const std::string& message) {
    log(LogLevel::DEBUG, message);
}

void Logger::info(const std::string& message) {
    log(LogLevel::INFO, message);
}

void Logger::warn(const std::string& message) {
    log(LogLevel::WARN, message);
}

void Logger::error(const std::string& message) {
    log(LogLevel::ERROR, message);
}

void Logger::fatal(const std::string& message) {
    log(LogLevel::FATAL, message);
}

std::string Logger::logLevelToString(LogLevel logLevel) {
    std::string result = "";

    if     (logLevel == LogLevel::TRACE) {result = "TRACE";}
    else if(logLevel == LogLevel::DEBUG) {result = "DEBUG";}
    else if(logLevel == LogLevel::INFO)  {result = "INFO";}
    else if(logLevel == LogLevel::WARN)  {result = "WARN";}
    else if(logLevel == LogLevel::ERROR) {result = "ERROR";}
    else if(logLevel == LogLevel::FATAL) {result = "FATAL";}
    else                                 {result = "TRACE";}

    return result;
}

LogLevel Logger::stringToLogLevel(const std::string& levelString) {
    if (levelString == "TRACE") {
        return LogLevel::TRACE;
    } else if (levelString == "DEBUG") {
        return LogLevel::DEBUG;
    } else if (levelString == "INFO") {
        return LogLevel::INFO;
    } else if (levelString == "WARN") {
        return LogLevel::WARN;
    } else if (levelString == "ERROR") {
        return LogLevel::ERROR;
    } else if (levelString == "FATAL") {
        return LogLevel::FATAL;
    } else {
        // Default to TRACE level if the string does not match any known log level
        return LogLevel::TRACE;
    }
}

Logger::~Logger() {
    m_logFile.close();
}