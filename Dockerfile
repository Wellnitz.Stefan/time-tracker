# using gcc base-image to hopefully have all necessary stuff included
FROM ubuntu:23.04

# install needed packages
RUN apt-get update && apt-get install -y \
    build-essential \
    g++ \
    wget \
    cppcheck \
    cmake \
    nano

# set workdir and copy files from host
WORKDIR /workdir
COPY . /workdir

# compile and install test-application
RUN cd /workdir && \
    ls && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    ls -hail

# start shell
CMD ["bash"]
